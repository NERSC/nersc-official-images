import os
import subprocess

registry = os.environ['CI_REGISTRY']

#find the files that have been changed in the most recent commit and write to file
find_changed_files = "git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA > changed-files.txt"
print(find_changed_files)
find_changed_files_output = subprocess.check_output(find_changed_files, shell=True)
#print(find_changed_files_output)

#now open the file we wrote
with open('changed-files.txt') as f:
    lines = [line.rstrip() for line in f]
print('changed lines:', lines)

#search changed files for containerfiles
containerfiles = []
for line in lines:
    if 'Containerfile' in line:
        containerfiles.append(line)
print('changed containerfiles:', containerfiles)        

#ok great, now we have a list of changed containerfiles. we only want to rebuild 
#the image if the containerfile has changed.

#now parse the containerfile lines to get the image name and tag
for containerfile in containerfiles:
    print("containerfile:", containerfile)

    #parse out our name and tag
    path_before_containerfile = containerfile.split("/Containerfile")[0]
    tag = path_before_containerfile.split("/")[-1]
    name = path_before_containerfile.split("/")[-2]
    print("now building {}:{}".format(name, tag))

    #now that we have the image name and tag, let's build
    podman_build = "podman build -t {}/library/nersc/{}:{} . --file {}".format(registry, name, tag, containerfile)                   
    print(podman_build)
    podman_build_output = subprocess.check_output(podman_build, shell=True)
    print(podman_build_output)

    #check to see what we have
    podman_images = subprocess.check_output("podman images", shell=True)
    print(podman_images)

    podman_push = "podman push --log-level=debug {}/library/nersc/{}:{}".format(registry, name, tag)
    print(podman_push)
    podman_push_output = subprocess.check_output(podman_push, shell=True)
    print(podman_push_output)
        

