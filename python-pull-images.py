import os
import subprocess
import time

#get list of all images and tags we expect to be on registry.nersc.gov
#for now we'll take the repo itself as the source of truth
#there is probably a better way to do this

registry = 'registry.nersc.gov/library/nersc/'

#find full paths of all dockerfiles in the repo
dockerfile_paths = subprocess.run(["find", ".", "-name", "Dockerfile"], capture_output=True, text=True).stdout.splitlines()
print("dockerfile_paths", dockerfile_paths)

#now parse the dockerfile_paths to get the image name and tag
for dockerfile in dockerfile_paths:

    #parse out our name and tag
    path_before_dockerfile = dockerfile.split("/Dockerfile")[0]
    tag = path_before_dockerfile.split("/")[-1]
    name = path_before_dockerfile.split("/")[-2]

    print("image {}:{}".format(name, tag))

    #wrap the whole thing in a try/except so that one failed image pull
    #won't tank the whole pipeline
    
    try:
        #no login needed since these images are public
        shifterimg_pull = ""
        shifterimg_pull_output = subprocess.run(["shifterimg", "pull", "{}{}:{}".format(registry, name, tag)], capture_output=True, text=True).stdout
        print(shifterimg_pull_output)
        time.sleep(1)
        
    except Exception as e:
        print("failed shifterimg pull for {}:{}".format(name, tag))
        print("error:", e)

