#!/bin/bash

# This script will be executed before our podman build 
# Copy amber tarfile from NERSC shared filesystem
# Tarfile is too big to store in gitlab and also behind a web form, so we can't wget it in the image

cp /global/common/software/nstaff/namehta4/amber20_src.tar.bz2 .
