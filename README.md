# README

Test repository for NERSC staff provided official images. 

Any changed Containerfile in the resposity will be automatically 
built on Perlmutter and pushed to registry.nersc.gov/library/nersc
by a gitlab runner.

Users can either look at the Containerfiles we have included or use the
finished products as base image and add their own software on top. For example:

```
FROM registry.nersc.gov/library/nersc/dask:2022.11.0-cpu

RUN conda install your fav packages

RUN ...
```

## Contributing

NERSC staff are welcome to open a PR to add Containerfiles for base images
they plan to support. Please follow naming/tagging guidelines:

```
/nersc/<software name>/<version-arch>
```

so the image will be named `software name` and tagged `version-arch`.

Please make changes **here** and not in the internal mirror, which is
only meant to connect to Perlmutter runners while allowing this reposity
to remain publicly viewable.

## CI

Note this repository is currently mirrored to software.nersc.gov/containers,
where it can access a runner on a Perlmutter login node. The runner uses
rootless Podman to build and push the images to registry.nersc.gov/library/nersc.


